from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from beauty.models.booking import Time, WorkingDays, Booking
from beauty.models.favorite import Favorite, Saved
from beauty.models.region import Region, District, Mahalla
from beauty.models.service import Category, Service


@admin.register(Region)
class RegionModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name")
    filter = ("name",)
    ordering = ("id",)


@admin.register(District)
class DistrictModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name", "region")
    ordering = ("id",)


@admin.register(Mahalla)
class MahallaModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name", "district")
    ordering = ("id",)


@admin.register(Category)
class CategoryModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name", "parent")


@admin.register(Service)
class CategoryModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name", "price", "duration", "description", "category", "user")


@admin.register(Favorite)
class FavoriteModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "service", "user", "like")


@admin.register(Saved)
class SavedModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "service", "user", "saved")


@admin.register(WorkingDays)
class WorkingDaysModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "day")
    ordering = ("id",)


@admin.register(Time)
class WorkingTimeModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "day", "start_time", "end_time", "user")
    ordering = ("id",)


@admin.register(Booking)
class BookingModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "date", "time",  "user", "status")
    ordering = ("id",)
