from rest_framework import serializers

from beauty.models.favorite import Favorite, Saved


class FavoriteSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Favorite
        fields = ('id', 'service', 'user', 'like')

    def validate(self, attrs):
        service = attrs.get('service')
        user = attrs.get('user')

        if Favorite.objects.filter(service=service, user=user).exists():
            raise serializers.ValidationError('You have already added this service to your favorites.')

        return attrs



class SavedSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Saved
        fields = ('id', 'service', 'user', 'saved')

    def validate(self, attrs):
        service = attrs.get('service')
        user = attrs.get('user')

        if Saved.objects.filter(service=service, user=user).exists():
            raise serializers.ValidationError('You have already saved this service.')

        return attrs
