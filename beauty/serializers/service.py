from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from beauty.models.favorite import Favorite, Saved
from beauty.models.service import Category, Service
from users.serializers import UserServiceModelSerializer


class CategoryModelSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'parent')


class ServiceModelSerializer(ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    image = serializers.ImageField(required=False)

    class Meta:
        model = Service
        fields = ('id', 'name', 'price', 'duration', 'description', 'category', "image", 'user')

    def validate(self, attrs):
        user = self.context['request'].user
        if not user.is_master:
            raise serializers.ValidationError('Only master can add posts')
        return attrs


class ServiceListSerializer(ModelSerializer):
    user = UserServiceModelSerializer()
    favorites_count = serializers.SerializerMethodField()

    is_like = serializers.SerializerMethodField()
    is_saved = serializers.SerializerMethodField()

    class Meta:
        model = Service
        fields = (
        'id', 'name', 'price', "image", 'category', 'duration', 'description', 'user', 'favorites_count', 'is_like',
        'is_saved')

    def get_favorites_count(self, obj):
        return Favorite.objects.filter(service=obj).count()

    def get_is_like(self, obj):
        user = self.context['request'].user
        if user.is_authenticated:
            return Favorite.objects.filter(service=obj, user=user).exists()
        return False

    def get_is_saved(self, obj):
        user = self.context['request'].user
        if user.is_authenticated:
            return Saved.objects.filter(service=obj, user=user).exists()
        return False
