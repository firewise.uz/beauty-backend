from django.db.models import *
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel


class Category(MPTTModel):
    name = CharField(max_length=255)
    parent = TreeForeignKey('self', on_delete=CASCADE, null=True, blank=True, related_name='children')

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        db_table = 'category'

    def __str__(self):
        return self.name


class Service(Model):
    name = CharField(max_length=255)
    price = DecimalField(max_digits=10, decimal_places=2)
    duration = CharField(max_length=10)
    description = TextField(null=True, blank=True)
    category = ForeignKey(Category, on_delete=CASCADE, related_name='services')
    user = ForeignKey('users.User', on_delete=CASCADE, related_name='services')
    image = ImageField(upload_to='service/')

    class Meta:
        verbose_name = 'Service'
        verbose_name_plural = 'Services'
        db_table = 'service'

    def __str__(self):
        return self.name
