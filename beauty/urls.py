from django.urls import path

from beauty.views.booking import (TimeListCreateAPIView, TimeUpdateDestroyAPIView, MasterFreeTimeListAPIView,
                                  BookingCreateAPIView, WorkingDayListAPIView, MyBookingListAPIView,
                                  BookingUpdateAPIView)
from beauty.views.favorite import FavoriteListCreateAPIView, SavedListCreateAPIView
from beauty.views.region import RegionListAPIView, DistrictListAPIView, MahallaListAPIView
from beauty.views.service import (CategoryListCreateAPIView, ServiceCreateAPIView,
                                  ServiceRetrieveUpdateDestroyAPIView, ServiceListAPIView)

urlpatterns = [
    path("region", RegionListAPIView.as_view()),
    path("district", DistrictListAPIView.as_view()),
    path("mahalla", MahallaListAPIView.as_view()),
    path("category", CategoryListCreateAPIView.as_view()),
    path("favorite", FavoriteListCreateAPIView.as_view()),
    path("saved", SavedListCreateAPIView.as_view()),
    path("service", ServiceCreateAPIView.as_view()),
    path("service/list", ServiceListAPIView.as_view()),
    path("service/<int:pk>", ServiceRetrieveUpdateDestroyAPIView.as_view()),
    path("working/day", WorkingDayListAPIView.as_view()),
    path("working/time", TimeListCreateAPIView.as_view()),
    path("working/time/<int:pk>", TimeUpdateDestroyAPIView.as_view()),
    path("booking/<int:pk>", BookingUpdateAPIView.as_view()),
    path("booking", BookingCreateAPIView.as_view()),
    path("booking/time", MasterFreeTimeListAPIView.as_view()),
    path("booking/my", MyBookingListAPIView.as_view()),

]
